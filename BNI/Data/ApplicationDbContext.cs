﻿using BNI.Models;
using Microsoft.EntityFrameworkCore;

namespace BNI.Data
{
    public class ApplicationDbContext :DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }  
    }
}
