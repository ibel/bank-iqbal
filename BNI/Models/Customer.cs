﻿using System.ComponentModel.DataAnnotations;

namespace BNI.Models
{
    public class Customer
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Customer_Name { get; set; }
        public string Address { get; set; }
        public string Place_Of_Birth { get; set; }
        public DateTime Date_Of_Birth { get; set; }
        public string Identity_Id { get; set; }
        public string Contact { get; set; }
    }
}
