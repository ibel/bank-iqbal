﻿using BNI.Data;
using BNI.Models;
using Microsoft.AspNetCore.Mvc;

namespace BNI.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ApplicationDbContext _db;

        public CustomerController(ApplicationDbContext db)
        {
            _db = db;   
        }
        public IActionResult Index()
        {
            IEnumerable<Customer> objCustomerList = _db.Customers;
            return View(objCustomerList);
        }

        //GET Data
        public IActionResult Create()
        {
            return View();
        }

        //POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Customer obj)
        {
            if (ModelState.IsValid)
            {
                _db.Customers.Add(obj);
                _db.SaveChanges();
                TempData["Success"] = "Customer Created Successfully ...!";
                return RedirectToAction("Index");
            }
            return View(obj);   
        }

        //GET Data For Edit
        public IActionResult Edit(int? Id)
        {
            if(Id == null || Id == 0)
            {
                return NotFound();  
            }
            var customer = _db.Customers.Find(Id);

            if (customer == null)
            {
                return NotFound();
            }
            return View(customer);
        }

        //Execute Edit Data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Customer obj)
        {
            if (ModelState.IsValid)
            {
                _db.Customers.Update(obj);
                _db.SaveChanges();
                TempData["Success"] = "Customer Successfully Update ...!";
                return RedirectToAction("Index");
            }
            return View(obj);
        }

        //GET Data For Delete
        public IActionResult Delete(int? Id)
        {
            if (Id == null || Id == 0)
            {
                return NotFound();
            }
            var customer = _db.Customers.Find(Id);

            if (customer == null)
            {
                return NotFound();
            }
            return View(customer);
        }

        //Execute Delete Data
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeletePOST(int? Id)
        {
            var obj = _db.Customers.Find(Id);
            if(obj == null)
            {
                return NotFound();
            }
            
            _db.Customers.Remove(obj);
            _db.SaveChanges();
            TempData["Success"] = "Customer Successfully Deleted ...!";

            return RedirectToAction("Index");
            
            return View(obj);
        }
    }
}
